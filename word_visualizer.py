import pytagcloud
import operator


def word_clouding(nounsDict):
    sorted_nounsDict = sorted(nounsDict.items(), key=operator.itemgetter(1), reverse=True)
    sorted_nounsDict = sorted_nounsDict[:500]
    taglist = pytagcloud.make_tags(sorted_nounsDict, maxsize=100)
    pytagcloud.create_tag_image(taglist, 'wordcloud.jpg', size=(900, 600), fontname='korean', rectangular=False)