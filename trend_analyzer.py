from web_crawler import *
from excel_handler import *
from morpheme_analyzer import *
from word_visualizer import *


print("Progress: Crawling a website")
title_list, hit_count_list = web_crawling()

print("Progress: Creating an excel report")
excel_handling(title_list, hit_count_list)

print("Progress: Analyzing words")
nounsDict = morpheme_analyzing(title_list, hit_count_list)

print("Progress: Visualizing trend analysis results")
word_clouding(nounsDict)

print("Done")


