from konlpy.tag import Twitter


def morpheme_analyzing(title_list, hit_count_list):
    nouns_list_dict = dict()
    for index in range(len(title_list)):
        # Analyze the nouns from the title sentence
        twitter = Twitter()
        nouns_list = twitter.nouns (title_list[index])
        for nouns in nouns_list:
            if nouns in nouns_list_dict:
                nouns_list_dict[nouns] = int(nouns_list_dict.get(nouns)) + int(hit_count_list[index])
            else:
                nouns_list_dict[nouns] = int(hit_count_list[index])
    return nouns_list_dict
