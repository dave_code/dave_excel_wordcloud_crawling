from urllib.request import urlopen
from bs4 import BeautifulSoup
import re


def web_crawling():
    hit_list = []
    title_list = []

    for pageNum in range(10):
        html = urlopen("https://www.seeko.co.kr/zboard4/zboard.php?id=mainnews&page=" + str(
            pageNum + 1) + "&select_arrange=headnum&desc=asc&category=&sn=off&ss=on&sc=off&keyword=&sn1=&divpage=10")
        html_object = BeautifulSoup(html, "html.parser")
        title_data = html_object.findAll("td", {"class": "article_subject"})
        title_hit = html_object.findAll("td", {"class": "article_count"})

        regular_exp = re.compile('\[[0-9]*\]')
        for title_wrap in title_data:
            title_list.append(regular_exp.sub('', title_wrap.get_text()).strip())
        for title_hit_warp in title_hit:
            hit_list.append(title_hit_warp.get_text().strip())

    return title_list, hit_list
